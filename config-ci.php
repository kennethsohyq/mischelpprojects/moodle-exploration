<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Template configuraton file for Gitlab CI
 * Duplicate of the GitHub Actions workflow
 *
 * @package    core
 * @copyright  2020 onwards Eloy Lafuente (stronk7) {@link https://stronk7.com}
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

unset($CFG);
global $CFG;
$CFG = new stdClass();

$dbuser = 'test';
if (getenv('dbuser') !== false) {
    $dbuser = getenv('dbuser');
}

$CFG->dbtype    = getenv('dbtype');
$CFG->dblibrary = 'native';
$CFG->dbhost    = getenv('dbhost');
$CFG->dbname    = 'test';
$CFG->dbuser    = $dbuser;
$CFG->dbpass    = 'P@ssw0rd';
$CFG->prefix    = 'm_';
$CFG->dboptions = ['dbcollation' => 'utf8mb4_bin', 'logall' => false, 'logerrors' => false];

$CFG->behat_dbtype    = getenv('dbtype');
$CFG->behat_dblibrary = 'native';
$CFG->behat_dbhost    = getenv('dbhost');                      // Database IP
$CFG->behat_dbname    = 'test';              // Database Schema for PHPUnit (MAKE SURE THIS EXISTS AS AN EMPTY DB)
$CFG->behat_dbuser    = $dbuser;
$CFG->behat_dbpass    = 'P@ssw0rd';
//$CFG->behat_dataroot = realpath(dirname(__DIR__)) . '/moodledata/behat';
$CFG->behat_dataroot = '/data/moodle/behat';
$CFG->behat_prefix = 'bht_';
$CFG->behat_wwwroot = 'http://127.0.0.1/moodle';
$CFG->behat_parallel_run = [
    [
       'dbtype' => getenv('dbtype'),
       'dblibrary' => 'native',
       'dbhost' => getenv('dbhost'),
       'dbname' => 'test',
       'dbuser' => $dbuser,
       'dbpass' => 'P@ssw0rd',
       'behat_prefix' => 'bhtp1_',
       'wd_host' => 'http://selenium:4444/wd/hub',
       'behat_wwwroot' => 'http://behat1.parallel/moodle',
       'behat_dataroot' => '/data/moodle/behatp1'
    ],[
        'dbtype' => getenv('dbtype'),
        'dblibrary' => 'native',
        'dbhost' => getenv('dbhost'),
        'dbname' => 'test',
        'dbuser' => $dbuser,
        'dbpass' => 'P@ssw0rd',
        'behat_prefix' => 'bhtp2_',
        'wd_host' => 'http://selenium:4444/wd/hub',
        'behat_wwwroot' => 'http://behat2.parallel/moodle',
        'behat_dataroot' => '/data/moodle/behatp2'
    ],[
        'dbtype' => getenv('dbtype'),
        'dblibrary' => 'native',
        'dbhost' => getenv('dbhost'),
        'dbname' => 'test',
        'dbuser' => $dbuser,
        'dbpass' => 'P@ssw0rd',
        'behat_prefix' => 'bhtp3_',
        'wd_host' => 'http://selenium:4444/wd/hub',
        'behat_wwwroot' => 'http://behat3.parallel/moodle',
        'behat_dataroot' => '/data/moodle/behatp3'
    ],[
        'dbtype' => getenv('dbtype'),
        'dblibrary' => 'native',
        'dbhost' => getenv('dbhost'),
        'dbname' => 'test',
        'dbuser' => $dbuser,
        'dbpass' => 'P@ssw0rd',
        'behat_prefix' => 'bhtp4_',
        'wd_host' => 'http://selenium:4444/wd/hub',
        'behat_wwwroot' => 'http://behat4.parallel/moodle',
        'behat_dataroot' => '/data/moodle/behatp4'
    ],
];

$CFG->wwwroot   = "http://localhost/moodle";
$CFG->dataroot  = realpath(dirname(__DIR__)) . '/moodledata';
$CFG->admin     = 'admin';
$CFG->directorypermissions = 0777;

// Debug options - possible to be controlled by flag in future.
$CFG->debug = 0; // DEBUG_DEVELOPER.
$CFG->debugdisplay = 0;
$CFG->debugstringids = 0; // Add strings=1 to url to get string ids.
//$CFG->perfdebug = 15;
//$CFG->debugpageinfo = 1;
$CFG->allowthemechangeonurl = 1;
$CFG->passwordpolicy = 0;
$CFG->cronclionly = 0;
$CFG->pathtophp = getenv('pathtophp');

$CFG->phpunit_dataroot  = realpath(dirname(__DIR__)) . '/phpunitdata';
$CFG->phpunit_prefix = 't_';

define('TEST_EXTERNAL_FILES_HTTP_URL', 'http://exttests');
define('TEST_EXTERNAL_FILES_HTTPS_URL', 'http://exttests');

define('TEST_SESSION_REDIS_HOST', 'redis');
define('TEST_CACHESTORE_REDIS_TESTSERVERS', 'redis');

// TODO: add others (solr, mongodb, memcached, ldap...).

// Too much for now: define('PHPUNIT_LONGTEST', true); // Only leaves a few tests out and they are run later by CI.

if (getenv('BEHAT_TESTING') !== false && getenv('BEHAT_TESTING') == 1) {
	require_once('/opt/moodle/moodle-browser-config/init.php');
}



require_once(__DIR__ . '/lib/setup.php');